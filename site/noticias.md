
# Noticias GuayaHack

%## 2023-07-17 

%## 2023-07-17 

%## 2023-07-17 

%## 2023-07-17 

%## 2023-07-17 

%## 2023-07-17 

%## 2023-07-17 

%## 2023-07-17 

%## 2023-07-17 

## 2023-07-20 

We got to know quite a few members and talk in English while on the Study Room 1 (Hi guys, this is a test MR!)

`~ Los del Study Room 1`

Se movió el calendario de `/wiki/calendario` a `/calendario`

`~ J`

## 2023-07-19 

Se eligió una fecha tentativa (`2023‐07‐22T15:30:00Z` | `2023‐07‐22T10:30:00−05:00`) para la inauguración del espacio y definción de forma de trabajo: 

```{figure} noticias.md-data/2023-07-19-evento-inauguracion.png
---
---
Evento de Inauguración, en el {doc}`/calendario`
```

Más información en {doc}`/wiki/evento-2023-07-22-inauguracion`

`~ J`

## 2023-07-17 

Se envian las primeras invitaciones y se asignan los primeros voluntarios se ofrecen como moderadores: {doc}`/community/member/jdsalaro/index`, 
%{doc}`/community/member/jdsalaro/index`, {doc}`/community/member/jdsalaro/index`, {doc}`/community/member/jdsalaro/index`, 

#todo:escribir agregar moderadores faltantes en la lista Stween, Chris, Isa!

Ese mismo día y por desparche se registra `guayahack.co`:

```console
jdsalaro@shell:/tmp$ whois guayahack.co
Domain Name: guayahack.co
Updated Date: 2023-07-17T13:39:25Z
Creation Date: 2023-07-17T13:39:22Z

```

## 2023-07-16 

`Sun, 16 Jul, 2023, 08:46:35 PM UTC` le dio a {doc}`/community/member/jdsalaro/index` por hacer un post en [/r/Colombia](https://www.reddit.com/r/Colombia/comments/151fkiz/con_una_prima_y_un_amigo_armaremos_un_grupo_de/). A raíz del interés de la gente local y quienes se encuentran en el exterior se crea el espacio en Discord.

`~ J`

